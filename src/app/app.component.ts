import { Component, OnInit } from '@angular/core';
import { Appointment, AppointmentType } from "./model/appointment.model";
import { addWeeks, endOfWeek, parse, startOfWeek, subWeeks } from "date-fns";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'time-planner';

  appointments: Appointment[] = [{
    id: 1,
    title: 'Kliendile lahenduste otsimine (tööandjaga, KOViga vmt)',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 08:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 09:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PURPLE
  }, {
    id: 2,
    title: 'Puhkus',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 09:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 09:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 3,
    title: 'Juku Siidisukk',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:05', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 4,
    title: 'Taivo Toores',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:05', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 5,
    title: 'Mikhail Mihhailov',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:15', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 6,
    title: 'Anatoli Golubtsov',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:15', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 7,
    title: 'Liisi Kriis',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:25', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 8,
    title: 'Mari Maasikas',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 11:25', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 9,
    title: 'Siim Sinipea',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 10,
    title: 'Aleksander-Skander Targaryen',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 11,
    title: 'Sten Kräsupea',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 12,
    title: 'Robert-Remi Oppar',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:50', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 13,
    title: 'Toomas Tõõra',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:50', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 11:55', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 14,
    title: 'Annika Jõesalu',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-09 11:55', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 15,
    title: 'Puhkus',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 12:15', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 16,
    title: 'Lõuna',
    location: 'Tähesaju',
    startDate: parse('2021-08-09 12:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-09 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 17,
    title: 'Juku Siidisukk',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-10 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-10 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 18,
    title: 'Taivo Toores',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-10 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-10 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 19,
    title: 'Mikhail Mihhailov',
    location: 'Tähesaju',
    startDate: parse('2021-08-10 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-10 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 20,
    title: 'Anatoli Golubtsov',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-10 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-10 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 21,
    title: 'Liisi Kriis',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-10 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-10 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 22,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-13 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-13 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.GREEN
  }, {
    id: 23,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-13 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-13 12:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.PINK
  }, {
    id: 24,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-13 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-13 13:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.PURPLE
  }, {
    id: 25,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-13 14:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-13 14:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.BLUE
  }, {
    id: 26,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-12 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-12 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.GREEN
  }, {
    id: 27,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-12 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-12 12:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.PINK
  }, {
    id: 28,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-12 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-12 13:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.PURPLE
  }, {
    id: 29,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-12 14:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-12 14:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.BLUE
  },{
    id: 30,
    title: 'Kliendile lahenduste otsimine (tööandjaga, KOViga vmt)',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 10:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PURPLE
  }, {
    id: 31,
    title: 'Puhkus',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 09:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 09:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 32,
    title: 'Juku Siidisukk',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:05', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 33,
    title: 'Taivo Toores',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:05', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 34,
    title: 'Mikhail Mihhailov',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:15', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 35,
    title: 'Anatoli Golubtsov',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:15', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 36,
    title: 'Liisi Kriis',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:25', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 37,
    title: 'Mari Maasikas',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 11:25', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 38,
    title: 'Siim Sinipea',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 39,
    title: 'Aleksander-Skander Targaryen',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 40,
    title: 'Sten Kräsupea',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 41,
    title: 'Robert-Remi Oppar',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:50', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 42,
    title: 'Toomas Tõõra',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:50', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 11:55', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 43,
    title: 'Annika Jõesalu',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-16 11:55', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 44,
    title: 'Puhkus',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 12:15', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 45,
    title: 'Lõuna',
    location: 'Tähesaju',
    startDate: parse('2021-08-16 12:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-16 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.PINK
  }, {
    id: 46,
    title: 'Juku Siidisukk',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-17 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-17 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 47,
    title: 'Taivo Toores',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-17 11:10', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-17 11:20', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 48,
    title: 'Mikhail Mihhailov',
    location: 'Tähesaju',
    startDate: parse('2021-08-17 11:30', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-17 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 49,
    title: 'Anatoli Golubtsov',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-17 11:35', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-17 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 50,
    title: 'Liisi Kriis',
    location: 'Telefoni kõne',
    startDate: parse('2021-08-17 11:40', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-17 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    type: AppointmentType.BLUE
  }, {
    id: 51,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-20 11:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-20 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.GREEN
  }, {
    id: 52,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-20 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-20 12:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.PINK
  }, {
    id: 53,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-20 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-20 13:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.PURPLE
  }, {
    id: 54,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-20 14:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-20 14:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    type: AppointmentType.BLUE
  }, {
    id: 55,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-18 10:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-18 11:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.GREEN
  }, {
    id: 56,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-19 12:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-19 12:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.PINK
  }, {
    id: 57,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-19 13:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-19 13:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.PURPLE
  }, {
    id: 58,
    title: 'EURES töötuba',
    location: 'Tähesaju',
    startDate: parse('2021-08-19 14:00', 'yyyy-MM-dd HH:mm', new Date()),
    endDate: parse('2021-08-19 14:45', 'yyyy-MM-dd HH:mm', new Date()),
    totalParticipants: 10,
    confirmedParticipants: 8,
    organizer: 'Remi-Gaillard Brudenshaften',
    striped: true,
    type: AppointmentType.BLUE
  }];
  startDate = new Date();

  appointmentChanged(appointment: Appointment) {
    this.appointments = this.appointments.map(a => {
      if (a.id === appointment.id) {
        return appointment;
      } else {
        return {...a};
      }
    });
  }

  getShownWeekText(): string {
    const start = startOfWeek(this.startDate, {weekStartsOn: 1});
    const end = endOfWeek(this.startDate, {weekStartsOn: 1});
    const startMonthText = start.toLocaleDateString('default', {month: 'long'});
    const endMonthText = end.toLocaleDateString('default', {month: 'long'});

    if (startMonthText !== endMonthText) {
      return `${start.getDate()}. ${startMonthText} - ${end.getDate()}. ${endMonthText}, ${this.startDate.getFullYear()}`
    } else {
      return `${start.getDate()}.-${end.getDate()}. ${startMonthText}, ${this.startDate.getFullYear()}`;
    }
  }

  showCurrentWeek() {
    this.startDate = new Date();
  }

  showPreviousWeek() {
    this.startDate = subWeeks(this.startDate, 1);
  }

  showNextWeek() {
    this.startDate = addWeeks(this.startDate, 1);
  }
}
