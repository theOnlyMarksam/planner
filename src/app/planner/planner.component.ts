import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import {
  addDays,
  addHours,
  isToday,
  parse,
  startOfWeek,
  startOfDay,
  isAfter,
  isEqual,
  isBefore,
  differenceInMinutes, addMinutes, setMinutes
} from 'date-fns';
import { Appointment } from "../model/appointment.model";

@Component({
  selector: 'app-planner',
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.scss']
})
export class PlannerComponent implements OnInit, OnChanges {
  @Output() appointmentChange = new EventEmitter<Appointment>();
  @Input() appointments: Appointment[] = [];
  @Input() startDate = new Date();

  days: Date[] = [...Array(7).keys()].map(i => addDays(startOfWeek(this.startDate, {weekStartsOn: 1}), i));
  hours: number[] = [...Array(24).keys()];
  activeHoursStart = 8;
  activeHoursEnd = 17;
  dragPrecisionMinutes = 5;

  showDragIndicator = false;
  dragIndicatorTime = '';
  dragIndicatorHour = 0;
  dragIndicatorOffset = '0px';

  private gridHeight = 100;
  private draggedAppointment: Appointment | null = null;
  private mouseOffsetFromDraggedElement = 0;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.startDate) {
      const newStartDate = changes.startDate.currentValue;
      this.days = [...Array(7).keys()].map(i => addDays(startOfWeek(newStartDate, {weekStartsOn: 1}), i));
    }
  }

  getDayName(d: Date): string {
    return d.toLocaleDateString('default', {weekday: 'long'});
  }

  today(d: Date): boolean {
    return isToday(d);
  }

  inactive(hour: number): boolean {
    return this.activeHoursStart > hour || this.activeHoursEnd <= hour;
  }

  getAppointmentsForHour(date: Date, hour: number): Appointment[] {
    const hourStart = addHours(startOfDay(date), hour);
    const hourEnd = addHours(hourStart, 1);

    return this.appointments.filter(app => isEqual(app.startDate, hourStart) ||
      (isAfter(app.startDate, hourStart) && isBefore(app.startDate, hourEnd)))
  }

  getColor(appointment: Appointment): string {
    return appointment.type.toLowerCase();
  }

  getAppointmentDynamicStyles(appointment: Appointment, day: Date, hour: number): {[key: string]: string} {
    const height = this.getHeight(appointment);
    const styles: {[key: string]: string} = {
      'height': height + 'px'
    };

    const smallAppointment = this.isSmallAppointment(appointment);
    if (appointment.endDate.getMinutes() === 30 && smallAppointment) {
      styles['bottom'] = this.gridHeight / 2 + 1 + 'px';
    } else if (appointment.endDate.getMinutes() === 0 && smallAppointment) {
      styles['bottom'] = '1px';
    } else {
      styles['top'] = this.getTop(appointment, day, hour, height);
    }

    if (this.isSmallAppointment(appointment)) {
      styles['font-size'] = '11px';
      styles['white-space'] = 'nowrap';
      styles['overflow'] = 'hidden';
    }

    const {visuallyOverlappingAppointments, appointmentOrder} = this.getVisuallyOverlappingAppointments(appointment);
    if (visuallyOverlappingAppointments.length > 0) {
      styles['width'] = (95 / 3) - 1 + '%';
      const col = (appointmentOrder % 3);
      styles['left'] = ((95 / 3) * col) + col + '%';
    }

    return styles
  }

  private getHeight(appointment: Appointment): number {
    const appointmentLength = differenceInMinutes(appointment.endDate, appointment.startDate);
    return Math.max((appointmentLength / 60 * this.gridHeight - 3), this.gridHeight / 6);
  }

  private getTop(appointment: Appointment, day: Date, hour: number, height: number): string {
    const hourStart = addHours(startOfDay(day), hour);
    const minutesToStart = differenceInMinutes(appointment.startDate, hourStart);

    const appointmentLength = differenceInMinutes(appointment.endDate, appointment.startDate);

    const top = (minutesToStart / 60 * this.gridHeight + 1);
    const mathematicalHeight = appointmentLength / 60 * this.gridHeight - 3;
    let adjustmentAmount = Math.max(height - mathematicalHeight, 0) / 2;

    if (appointment.startDate.getMinutes() === 0 || appointment.startDate.getMinutes() === 30) {
      adjustmentAmount = 0;
    }

    return  top - adjustmentAmount + 'px';
  }

  isSmallAppointment(appointment: Appointment): boolean {
    const appointmentLength = differenceInMinutes(appointment.endDate, appointment.startDate);
    return (appointmentLength / 60 * this.gridHeight - 3) <= this.gridHeight / 6;
  }

  private getVisuallyOverlappingAppointments(appointment: Appointment): {visuallyOverlappingAppointments: Appointment[], appointmentOrder: number} {
    const minutes = appointment.startDate.getMinutes();
    let start: Date;

    if (minutes < 30) {
      start = setMinutes(appointment.startDate, 0);
    } else {
      start = setMinutes(appointment.startDate, 30);
    }
    const end = addMinutes(start, 30);

    const appointmentsInCurrentHalfHour = this.appointments.filter(a => {
      return a.id !== appointment.id && (isEqual(a.startDate, start)
        || (isAfter(a.startDate, start) && isBefore(a.startDate, end)));
    });
    const order = appointmentsInCurrentHalfHour.filter(a => isBefore(a.startDate, appointment.startDate)).length;

    return {
      visuallyOverlappingAppointments: appointmentsInCurrentHalfHour,
      appointmentOrder: order
    }
  }

  dropAppointment(e: DragEvent, day: Date) {
    e.preventDefault();
    this.showDragIndicator = false;

    if (this.draggedAppointment) {
      const appointmentLengthMinutes = differenceInMinutes(this.draggedAppointment.endDate, this.draggedAppointment.startDate);
      const newStartTime = parse(this.dragIndicatorTime, 'HH:mm', day);
      const newEndTime = addMinutes(newStartTime, appointmentLengthMinutes);
      const newAppointment: Appointment = {
        ...this.draggedAppointment,
        startDate: newStartTime,
        endDate: newEndTime
      }
      this.appointmentChange.emit(newAppointment);
    }

    return false;
  }

  dragOver(e: DragEvent, day: Date, hour: number) {
    e.preventDefault();
    const dayHourElement = e.composedPath().find(obj => (obj as Element).className.includes('day-hour')) as Element;
    const dayHourElementPosition = dayHourElement.getBoundingClientRect();

    const divider = (this.gridHeight / 60 * this.dragPrecisionMinutes);
    const offset = Math.floor((e.y - this.mouseOffsetFromDraggedElement - dayHourElementPosition.y) / divider) * divider;

    let minutes = Math.round(offset / this.gridHeight * 60);
    let subtractFromHours = 0;
    if (minutes < 0) {
      minutes = 60 + minutes;
      subtractFromHours = 1;
    }

    this.showDragIndicator = true;
    this.dragIndicatorHour = hour;
    this.dragIndicatorOffset = offset + 'px';
    this.dragIndicatorTime = String(hour + (Math.floor(minutes / 60)) - subtractFromHours).padStart(2, '0') + ':' +
      String(minutes % 60).padStart(2, '0');

    return false;
  }

  hasCurrentTimeIndicator(day: Date, hour: number): boolean {
    const currentTime = new Date();
    const hourStart = addHours(startOfDay(day), hour);
    const hourEnd = addHours(hourStart, 1);

    return isEqual(currentTime, hourStart) ||
      (isAfter(currentTime, hourStart) && isBefore(currentTime, hourEnd))
  }

  getCurrentTimeIndicatorOffset(day: Date, hour: number): string {
    const currentTime = new Date();
    const hourStart = addHours(startOfDay(day), hour);

    return (differenceInMinutes(currentTime, hourStart) / 60 * this.gridHeight) + 'px';
  }

  appointmentDragStart(appointment: Appointment, event: DragEvent) {
    this.draggedAppointment = appointment;
    const draggedElement = event.composedPath()[0] as Element;
    const draggedElementRect = draggedElement.getBoundingClientRect();
    this.mouseOffsetFromDraggedElement = event.y - draggedElementRect.y;
  }

  hasParticipants(appointment: Appointment): boolean {
    return !isNaN(Number(appointment.totalParticipants));
  }

  getParticipantCountText(appointment: Appointment): string {
    const confirmed = appointment.confirmedParticipants ? appointment.confirmedParticipants : 0;
    const total = appointment.totalParticipants ? appointment.totalParticipants : 0;

    return confirmed + '/' + total;
  }
}
