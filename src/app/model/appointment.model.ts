export interface Appointment {
  id: number;
  title: string;
  location?: string;
  organizer?: string;
  startDate: Date;
  endDate: Date;
  striped?: boolean;
  type: AppointmentType;
  totalParticipants?: number;
  confirmedParticipants?: number;
}

export enum AppointmentType {
  PINK = 'PINK',
  PURPLE = 'PURPLE',
  BLUE = 'BLUE',
  GREEN = 'GREEN'
}
